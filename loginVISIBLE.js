const puppeteer = require('puppeteer');
(async () => {
  const browser = await puppeteer.launch({ headless: false })  // headless switch true/false
  const page = await browser.newPage()

  const navigationPromise = page.waitForNavigation()

  await page.goto('http://192.168.1.1');  // Go to router page
  await navigationPromise

  await page.waitForSelector('.login_frame > .content > .login_ul_1 > .login_li_2 > #Frm_Username')
  await page.click('.login_frame > .content > .login_ul_1 > .login_li_2 > #Frm_Username')
  await page.type('.login_frame > .content > .login_ul_1 > .login_li_2 > #Frm_Username', process.argv[2])  // VAR $USERNAME


  await page.waitForSelector('.login_frame > .content > .login_ul_1 > .login_li_2 > #Frm_Password')
  await page.click('.login_frame > .content > .login_ul_1 > .login_li_2 > #Frm_Password')
  await page.type('.login_frame > .content > .login_ul_1 > .login_li_2 > #Frm_Password', process.argv[3]) // VAR $PASSWORD

  await page.waitForSelector('.login_frame > .content > .login_ul_1 > .login_li_3 > #LoginId')
  await page.click('.login_frame > .content > .login_ul_1 > .login_li_3 > #LoginId')  // Click login button

  await navigationPromise
  await page.waitFor(1*1000); // Wait so that human eye can see what happened
  console.log(page.url()+ "\t" + "Username: " + process.argv[2] + "\t" + "Password: " + process.argv[3]);  // Get URL, we are looking for start.ghtml
  await browser.close()
})()

FROM node:9-alpine
MAINTAINER demoz
#Proxy
#ENV http_proxy http://IP:PORT
#ENV https_proxy http://IP:PORT
#General updates and installs
RUN  apk update && apk upgrade && apk add --update nodejs nodejs-npm 

# Needs solution for working instance of puppeteer

RUN npm i puppeteer
RUN apk add curl alpine-sdk bash
RUN curl https://kent.dl.sourceforge.net/project/crunch-wordlist/crunch-wordlist/crunch-3.6.tgz > crunch.tgz && tar -xzvf crunch.tgz && cd crunch-* && make && cp crunch /bin/
COPY login.js launcher.sh / 

# ZTE F660 WEB BruteForce

ZTE F660 brute force using Puppeteer https://github.com/GoogleChrome/puppeteer


<p align="center">
  <img src="https://gitlab.com/demoz/zte-f660-bruteforce/raw/master/img/bfa.png" alt="BFA"/>
</p>

## Proof of concept
 <p align="center">
  <img src="https://gitlab.com/demoz/zte-f660-bruteforce/raw/master/img/ztef660bfa2.gif" alt="proof of concept" />
</p>

## Getting Started

### Requirements nodejs & npm, crunch.

* https://nodejs.org/en/download/
* https://sourceforge.net/projects/crunch-wordlist/

Optional:

* https://git-scm.com/downloads
Git Bash on Windows to run launcher.

## Installation

To use Puppeteer in your project, run:

```bash
npm i puppeteer
# or "yarn add puppeteer"
```

Note: When you install Puppeteer, it downloads a recent version of Chromium (~170MB Mac, ~282MB Linux, ~280MB Win) that is guaranteed to work with the API. To skip the download, see [Environment variables](https://github.com/GoogleChrome/puppeteer/blob/v1.11.0/docs/api.md#environment-variables).


## Usage

1. Clone repository

`git clone git@gitlab.com:demoz/zte-f660-bruteforce.git`

2. Running script

inside zte-f660-bruteforce folder run

`bash launcher.sh <MINNUM> <MAXNUM>`

Where **<MINNUM>** & **<MAXNUM>** is a variable which defines minimum number and maximal number of characters for crunch to process.


## Docker way [EXPERIMENTAL/NOT STABLE]

1. Clone repository

2. Build docker image

Inside zte-f660-bruteforce folder run

`docker build -t bfa .`

3. Run container
 

`docker run --name bfa -d -it bfa /bin/sh`

4. Attach to container
 

`docker exec -it bfa sh`

5. Start bruteforcing 

`sh launcher.sh <MINNUM> <MAXNUM>`

Where **<MINNUM>** & **<MAXNUM>** is a variable which defines minimum number and maximal number of characters for crunch to process.`

### How to know that login is successful :

Unsuccessful:

```
node login.js blah blah
F660
http://192.168.1.1/
blah
```


Successful:

```
node login.js telekom telekom
F660
http://192.168.1.1/start.ghtml
telekom
```

The key is that once you are logged in, you are redirected to page *start.ghtml* .


## DEBUGGING

Run `nodejs loginVISIBLE.js $USER $PASSWORD` to have visual on what is going on in background.


## TODO

1. [x] System to generate passwords - general idea is to " **|** " pipe crunch or similar solution directly inside script

1. [ ] Mechanism for 2x admin login & 1x user login (telekom/telekom) to bypass cooldown/bruteforce protection, or maybe implement parralel for 1x/1x.

1. [x] Launcher script.

1. [ ] Docker image for single run command, packaged with all requirements.

1. [ ] Better docs.
